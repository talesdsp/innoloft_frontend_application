## Run live

This is project is deployed on netlify:
[https://eager-perlman-2a4141.netlify.com/](https://eager-perlman-2a4141.netlify.com/)

## How to run locally

*  clone this repository and open with your editor
*  on terminal run `cd innoloft`
*  run `yarn` or `npm i` to install all dependencies
*  run `yarn start` or `npm start` and wait.

CRA will automatically open the app on your default browser.


## Requirements

In first tab (basic data)

- [x] Change e-mail address
Change Password
The password must have certain properties:
- [x] "Password" and "Password repeat" fields need to be identical (including an indicator for this equality)
The password field should accept Uppercase letters, lowercase letters, numbers and special characters
A multi-color password strength indicator should be implemented
- [x] Button to update the user data

In the second tab (Address):

- [x] Change first name
- [x] Change Last Name
- [x] Change address (street, house number, postal code)
- [x] Change country (Germany, Austria, Switzerland are available)
- [x] Button to update the user data

Obs:
- [x] Please do not use Bootsrap or similar frameworks for CSS, just create new CSS from scratch.

>  Please note that this dashboard should be responsive, and be usable on mobile and tablets as well.
When styling, you can decide for yourself what this dashboard should look like.
However, bonus points are awarded if the styles resemble those of energieloft.de and could fit as seamlessly as possible within that website.
A basic layout mockup can be found in the file: mockup.jpg.

> The application should at the very least use the following:

- [x] React.js framework
- [x] A CSS pre-compiler (SASS, LESS, SCSS) or other CSS approaches (CSS modules, Styled components)


### Deadline
> You have a week to work on this project, just note down in the end how many hours it took to complete (roughly), as this can be an interesting matter of discussion.
Thank you, and have fun!

- [x] 3 days - about 9-10 hours


## Libs

### [Unform](https://github.com/Rocketseat/unform)
Unform is a performance focused library that helps you creating beautiful forms in React with the power of uncontrolled components performance and React Hooks.

### [Yup](https://github.com/jquense/yup)
Yup is a JavaScript schema builder for value parsing and validation. Define a schema, transform a value to match, validate the shape of an existing value, or both. Yup schema are extremely expressive and allow modeling complex, interdependent validations, or value transformations.

### Redux
Container to manage state across components.

### React-Redux
Specific react-redux bindings to perform communication among the two.

### Styled-Components
Component based CSS-in-JS with superpowers.

### [Clipboard.js](https://github.com/zenorocha/clipboard.js)
Simple 3kb library to add click-to-copy functionality