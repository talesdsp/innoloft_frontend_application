import ClipboardJS from "clipboard";
import React, {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as Yup from "yup";
import {formCreators} from "../store/ducks/form";
import clipBoard from "../svg/clipboard.svg";
import Buttons from "./Buttons";
import Input from "./Input";
import * as S from "./styled";

export default function BasicForm({active}) {
  const [strength, setStrength] = useState("null");
  const [equality, setEquality] = useState("null");

  const formRef = useRef(null);

  const [data, setData] = useState({
    email: "tales@gmail.com"
  });
  console.log({data});
  const dispatch = useDispatch();

  const {form} = useSelector((state) => state.formReducer);

  useEffect(() => {
    setTimeout(() => {
      formRef.current.setData(data);
    }, 2000);
  }, [data]);

  useEffect(() => {
    console.log(`%cPassword`, "font-size: 2rem; color: green", {strength});
  }, [strength]);

  const handleCancel = () => {
    formRef.current.setData(data);
    dispatch(formCreators.closeForm());
  };

  const handleSubmit = async (data) => {
    try {
      const schema = Yup.object().shape({
        email: Yup.string()
          .email("email is not valid")
          .required("email is required"),
        password: Yup.string()
          .required("This field is required")
          .min(8, "Password must have at least 8 characters")
          .max(16, "Password max characters is 16"),
        confirmPassword: Yup.string()
          .required("This field is required")
          .oneOf([Yup.ref("password"), null], "Passwords must match")
      });

      await schema.validate(data, {
        abortEarly: false
      });

      setData(data);
      dispatch(formCreators.closeForm());
      console.log({data});
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};
        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  const passwordStrength = (e) => {
    let points = 0;
    const password = e.target.value;

    const have8chars = (p) => p.match(/[\w\d!&%$#@<>;.,* -/\\]{8}/g) && 5;
    const have10chars = (p) => p.match(/[\w\d!&%$#@<>;.,* -/\\]{10}/g) && 1;
    const have16chars = (p) => p.match(/[\w\d!&%$#@<>;.,* -/\\]{16}/g) && 5;
    const haveSpecial = (p) => p.match(/[!&%$#@<>;.,* -/\\]/g) && 1;
    const haveLowerCase = (p) => p.match(/[a-z]/g) && 1;
    const haveUpperCase = (p) => p.match(/[A-Z]/g) && 1;
    const haveNumber = (p) => p.match(/[\d]/g) && 1;

    points += have8chars(password);
    points += have10chars(password);
    points += have16chars(password);
    points += haveNumber(password);
    points += haveSpecial(password);
    points += haveUpperCase(password);
    points += haveLowerCase(password);

    if (points >= 15) return "adamantium";
    else if (points >= 10) return "strong";
    else if (points >= 8) return "solid";
    else if (points >= 6) return "medium";
    else if (points >= 1) return "weak";
    else return "null";
  };

  const checkPasswordEquality = (e) => {
    const confirm = e.target.value;
    const password = document.getElementById("password").value;
    if (confirm !== password) return "not";
    else if (confirm === password) return "equal";
  };

  new ClipboardJS(".btn");

  return (
    <S.Unform ref={formRef} active={active} onSubmit={handleSubmit}>
      <S.FieldSet>
        <Input label="Email*" name="email" />
      </S.FieldSet>
      <S.FieldSet strength={strength} equality={equality} form={form === "done" ? 1 : 0}>
        <Input
          onChange={(e) => setStrength(passwordStrength(e))}
          label="Password*"
          type="password"
          name="password"
          id="password"
          placeholder="Hidden for security purposes"
          watch
        />

        <S.FieldSet column>
          <S.Label password update={form === "edit" ? true : false} htmlFor="confirmPassword">
            Confirm*
          </S.Label>
          <Input
            password
            type="password"
            name="confirmPassword"
            onChange={(e) => setEquality(checkPasswordEquality(e))}
          />
        </S.FieldSet>
      </S.FieldSet>
      <S.Rules password update={form === "edit" ? true : false}>
        <S.Rule head>Score</S.Rule>
        <S.Rule>required 8-16 chars</S.Rule>
        <S.Rule>bonus - 10 chars</S.Rule>
        <S.Rule>bonus - 16 chars</S.Rule>
        <S.Rule>bonus - special char eg: ! &amp; % $ # @ : &lt; &gt; ; . * , - / \</S.Rule>
        <S.Rule>bonus - uppercase char</S.Rule>
        <S.Rule>bouns - lowercase char</S.Rule>
        <S.Rule>bonus - number</S.Rule>
        <S.Test>Sample:</S.Test>
        <S.Test>
          <S.TestPassword id="copyText" value="1234567890123qQ!" />
          <S.TestButton type="button" className="btn" data-clipboard-target="#copyText">
            <img src={clipBoard} alt="Clipboard" />
          </S.TestButton>
        </S.Test>
      </S.Rules>
      <Buttons handleCancel={handleCancel} />
    </S.Unform>
  );
}
