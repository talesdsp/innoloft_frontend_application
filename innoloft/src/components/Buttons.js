import React from "react";
import {useSelector} from "react-redux";
import * as S from "./styled";

export default function Buttons({handleCancel}) {
  const {form} = useSelector((state) => state.formReducer);
  return (
    <S.Buttons show={form === "edit" ? true : false}>
      <S.Button type="submit">Update</S.Button>
      <S.Button type="button" cancel onClick={handleCancel}>
        Cancel
      </S.Button>
    </S.Buttons>
  );
}
