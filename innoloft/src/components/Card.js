import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import styled, {css} from "styled-components";
import {formCreators} from "../store/ducks/form";
import AddressForm from "./AddressForm";
import BasicForm from "./BasicForm";

export default function Card() {
  const [tab, setTab] = useState(1);

  const dispatch = useDispatch();
  const close = () => dispatch(formCreators.closeForm());

  const {form} = useSelector((state) => state.formReducer);
  const isTab1 = tab === 1 ? 1 : 0;
  const isTab2 = tab === 2 ? 1 : 0;

  const handleClick = (n) => {
    setTab(n);
    if (form === "done") return;
    close();
  };

  return (
    <FormContainer>
      <Tab>
        <Tab1 active={isTab1} onClick={() => handleClick(1)}>
          Main Information
        </Tab1>
        <Tab2 active={isTab2} onClick={() => handleClick(2)}>
          Additional Information
        </Tab2>
      </Tab>
      <BasicForm active={isTab1} />

      <AddressForm active={isTab2} />
    </FormContainer>
  );
}

const FormContainer = styled.div`
  padding: 7rem 0rem 3.4rem;
  position: absolute;
  margin: 0 auto;
  display: flex;
  border-radius: 1.2rem;
  overflow: hidden;
  left: 0;
  right: 0;
  background-color: #fff;
  width: 90%;
  align-items: center;
  justify-content: center;
  box-shadow: 0.2rem 0.4rem 1rem rgba(0, 0, 0, 0.2);

  @media (min-width: 768px) {
    padding: 8.9rem 0rem 5.5rem;
    width: 50%;
  }
`;

const Tab = styled.div`
  position: absolute;
  display: flex;
  flex-direction: row;
  height: 5rem;
  width: 100%;
  color: #000;
  font-size: 1.6rem;
  top: 0;
  left: 0;
  text-align: center;
  align-items: center;
  justify-content: center;
  background: rgba(40, 119, 98, 1);
  background: #bbb;
  cursor: pointer;
  div {
    height: 100%;
    padding: 1.25rem 0;
    margin: auto;
    flex: 1;
  }
`;

const Tab1 = styled.div`
  ${(props) =>
    props.active &&
    css`
      background-color: #fff;
    `};
`;

const Tab2 = styled.div`
  ${(props) =>
    props.active &&
    css`
      background-color: #fff;
    `};
`;
