import React from "react";
import {useDispatch, useSelector} from "react-redux";
import styled, {css} from "styled-components";
import {menuCreators} from "../store/ducks/menu";

export default function Menu() {
  const dispatch = useDispatch();
  const {menu} = useSelector((state) => state.menuReducer);

  const handleClick = () => {
    if (menu === "open") dispatch(menuCreators.closeMenu());
    else if (menu === "close") dispatch(menuCreators.openMenu());
  };
  return (
    <MENUBTN onClick={handleClick}>
      <MENUBTN__BURGER menu={menu}></MENUBTN__BURGER>
    </MENUBTN>
  );
}

const MENUBTN = styled.div`
  width: 5rem;
  height: 5rem;
  position: fixed;
  bottom: 3rem;
  right: 3rem;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: var(--special);
  border-radius: 50%;
  cursor: pointer;
  z-index: 102;
  overflow: hidden;
  @media (min-width: 768px) {
    display: none;
  }
`;

const MENUBTN__BURGER = styled.div`
  height: 0.3rem;
  width: 3rem;
  background: #fff;
  transition: 650ms transform ease;
  ${(props) =>
    props.menu === "open" &&
    css`
      transform: translate(6rem, 0);
    `}
  &::before,
  &::after {
    content: "";
    position: absolute;
    background: inherit;
    width: inherit;
    opacity: 1;
    height: inherit;
    transition: inherit;
  }

  &::before {
    transform: translateY(-0.8rem);

    ${(props) =>
      props.menu === "open" &&
      css`
        transform: translate(-6rem, 0) rotateZ(45deg);
      `}
  }

  &::after {
    transform: translateY(0.8rem);
    ${(props) =>
      props.menu === "open" &&
      css`
        transform-origin: center;
        transform: translate(-6rem, 0) rotateZ(-45deg);
      `}
  }
`;
