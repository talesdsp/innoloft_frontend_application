import React from "react";
import {useSelector} from "react-redux";
import styled, {css} from "styled-components";

export default function SideBar() {
  const {menu} = useSelector((state) => state.menuReducer);
  return (
    <Aside menu={menu}>
      <Item>
        <Icon></Icon> Home
      </Item>
      <Item>
        <Icon></Icon> My Account
      </Item>
      <Item>
        <Icon></Icon> My Company
      </Item>
      <Item>
        <Icon></Icon> My Settings
      </Item>
      <Item>
        <Icon></Icon> News
      </Item>
      <Item>
        <Icon></Icon> Analytics
      </Item>
    </Aside>
  );
}

const Aside = styled.aside`
  width: 100vw;
  position: fixed;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #fff;

  position: fixed;
  bottom: -100vh;
  overflow: hidden;
  border-top-right-radius: 100%;
  border-top-left-radius: 100%;
  transition: all 650ms ease;
  z-index: 101;

  ${(props) =>
    props.menu === "open" &&
    css`
      bottom: 0;
      border-radius: initial;
      background-color: rgba(230, 230, 230, 0.9);
      left: 0;
    `}
  @media (min-width: 768px) {
    width: max-content;
    display: block;
    height: initial;
    bottom: initial;
    border-radius: initial;
    left: 8rem;
    position: absolute;
    top: initial;
    z-index: initial;
  }
`;

const Item = styled.div`
  margin: 2rem 0;
  font-size: 1.6rem;
  width: fit-content;
  color: #000;
  cursor: pointer;
  :hover {
    color: rgba(40, 119, 98, 1);
  }
`;

const Icon = styled.i`
  color: #000;
`;
