import React from "react";
import styled from "styled-components";

export default function Footer() {
  return <Foot></Foot>;
}

const Foot = styled.footer`
  width: 100vw;
  height: 9rem;
  margin-top: 2rem;
  padding: 1px;
  bottom: 0;
  position: relative;
  left: 0;
  background-color: var(--main);
`;
