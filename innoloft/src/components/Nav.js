import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import styled, {keyframes} from "styled-components";
import {messageCreators} from "../store/ducks/message";
import {notificationCreators} from "../store/ducks/notification";
import Bell from "../svg/bell.svg";
import Globe from "../svg/globe.svg";
import Inbox from "../svg/inbox.svg";

export default function Navbar() {
  const [{messages}, {notifications}] = useSelector((state) => [
    state.messageReducer,
    state.notificationReducer
  ]);

  useEffect(() => {
    console.log(`%cFakeApi`, "font-size: 2rem; color: yellow", {messages});
  }, [messages]);
  useEffect(() => {
    console.log(`%cFakeApi`, "font-size: 2rem; color: yellow", {notifications});
  }, [notifications]);

  const dispatch = useDispatch();
  useEffect(() => {
    setInterval(
      () => dispatch(messageCreators.receiveMessage({from: "Tales", message: "whassup"})),
      7000
    );
    setInterval(
      () =>
        dispatch(
          notificationCreators.receiveNotification({
            title: "Special promo",
            message: "Go and check our.."
          })
        ),
      10000
    );
  }, [dispatch]);

  const readMessages = () => {
    if (messages.length === 0) return;
    return dispatch(messageCreators.readMessage());
  };

  const readNotifications = () => {
    if (notifications.length === 0) return;
    return dispatch(notificationCreators.readNotification());
  };

  return (
    <Nav>
      <Brand>
        ENER<G>G</G>IE
      </Brand>
      <Badge>LOFT</Badge>

      <Icons>
        <Icon>
          <img src={Globe} alt="World globe" />
          EN
        </Icon>

        <Icon onClick={readMessages}>
          <img src={Inbox} alt="Message inbox" title="click me" />
          {messages.length > 0 && <Counter>{messages.length > 99 ? 99 : messages.length}</Counter>}
        </Icon>

        <Icon onClick={readNotifications}>
          <img src={Bell} alt="Notification bell" title="click me" />
          {notifications.length > 0 && (
            <Counter>{notifications.length > 99 ? 99 : notifications.length}</Counter>
          )}
        </Icon>
      </Icons>
    </Nav>
  );
}

const catchUser = keyframes`
0%, 100%{
transform: scale(1);
}50%{
  transform: scale(2);
}
`;

const Counter = styled.div`
  position: absolute;
  width: 1.7rem;
  height: 1.7rem;
  border-radius: 50%;
  text-align: center;
  color: #000;
  font-size: 1.3rem;
  background-color: var(--special);
  margin: auto;
  right: -1rem;
  bottom: -1rem;
  animation: 1s ${catchUser} cubic-bezier(1, 0, 0, 1);
`;

const Icons = styled.div`
  display: flex;
  position: absolute;
  margin: auto;
  align-items: center;
  flex-direction: row;
  right: 2rem;
  @media (min-width: 768px) {
    right: 4rem;
  }
`;

const Icon = styled.i`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-size: 1.3rem;
  margin: 0 1rem;
  position: relative;
  color: #fff;
  cursor: pointer;
`;

const G = styled.span`
  color: #fff;
`;

const Brand = styled.h1`
  font-size: 2.5rem;
  color: var(--main);
  @media (min-width: 768px) {
    font-size: 3.4rem;
  }
`;

const Badge = styled.span`
  font-size: 1.1rem;
  font-weight: bold;
  color: var(--main);
  border: 0.3rem solid var(--main);
  margin-left: 1rem;
  @media (min-width: 768px) {
    font-size: 1.3rem;
  }
`;

export const Nav = styled.nav`
  position: fixed;
  z-index: 100;
  padding: 2rem 4rem;
  height: 9rem;
  top: 0;
  left: 0;
  width: 100vw;
  display: flex;
  flex-direction: row;
  align-items: center;
  background: rgb(2, 0, 36);
  background: linear-gradient(90deg, var(--alternate) 0%, var(--main) 100%);
`;
