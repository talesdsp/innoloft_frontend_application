import {Scope} from "@unform/core";
import React, {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as Yup from "yup";
import {formCreators} from "../store/ducks/form";
import Buttons from "./Buttons";
import Input from "./Input";
import * as S from "./styled";

export default function AdressForm({active}) {
  const formRef = useRef(null);
  const [data, setData] = useState({
    firstName: "Tales",
    lastName: "Pereira",
    address: {
      country: "Austria",
      street: "St New Avenue",
      number: 32,
      zipCode: "981077"
    }
  });
  console.log({data});

  const dispatch = useDispatch();
  const {form} = useSelector((state) => state.formReducer);

  useEffect(() => {
    setTimeout(() => {
      formRef.current.setData(data);
      document.querySelector("select").value = data.address.country;
    }, 2000);
  }, [data]);

  const openForm = () => dispatch(formCreators.openForm());

  const handleCancel = () => {
    formRef.current.setData(data);
    dispatch(formCreators.closeForm());
  };

  const handleSubmit = async (data) => {
    const justNumber = /([^\Wa-zA-Z])+$/g;
    try {
      const schema = Yup.object().shape({
        firstName: Yup.string().required("required"),
        lastName: Yup.string().required("required"),
        address: Yup.object().shape({
          street: Yup.string(),
          number: Yup.string().matches(justNumber, "Just accepts number"),
          zipCode: Yup.string().matches(justNumber, "Just accepts number")
        })
      });

      await schema.validate(data, {
        abortEarly: false
      });

      const setCountry = document.querySelector("select").value;

      setData({...data, address: {...data.address, country: setCountry}});
      dispatch(formCreators.closeForm());
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};
        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  return (
    <S.Unform ref={formRef} active={active} onSubmit={handleSubmit}>
      <S.FieldSet>
        <Input label="First Name*" name="firstName" />
        <Input label="Last Name*" name="lastName" />
      </S.FieldSet>

      <Scope path="address">
        <S.FieldSet>
          <Input label="Street" name="street" />
        </S.FieldSet>
        <S.FieldSet>
          <Input label="Number" name="number" />
          <Input label="Zip Code" name="zipCode" />
          <S.FieldSet column country>
            <S.Label onClick={openForm} htmlFor="country">
              Country*
            </S.Label>
            <S.Select
              onClick={openForm}
              update={form === "edit" ? true : false}
              name="country"
              id="country"
            >
              <option value="Germany">Germany</option>
              <option value="Austria">Austria</option>
              <option value="Switzerland">Switzerland</option>
            </S.Select>
          </S.FieldSet>
        </S.FieldSet>
      </Scope>

      <Buttons handleCancel={handleCancel} />
    </S.Unform>
  );
}
