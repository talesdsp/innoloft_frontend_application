import {useField} from "@unform/core";
import React, {useEffect, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {formCreators} from "../store/ducks/form";
import * as S from "./styled";

export default function Input({name, label, ...rest}) {
  const inputRef = useRef(null);
  const {fieldName, registerField, defaultValue, error} = useField(name);
  const {form} = useSelector((state) => state.formReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: "value"
    });
  }, [fieldName, registerField]);

  const handleClick = () => {
    if (form === "edit") return;
    dispatch(formCreators.openForm());
  };

  return (
    <S.FieldSet column>
      {label && (
        <S.Label onClick={handleClick} htmlFor={fieldName}>
          {label}
        </S.Label>
      )}
      <S.Input
        onClick={handleClick}
        update={form === "edit" ? true : false}
        id={fieldName}
        type="text"
        ref={inputRef}
        defaultValue={defaultValue}
        {...rest}
      />
      {error && form === "edit" && <S.Error>{error}</S.Error>}
    </S.FieldSet>
  );
}
