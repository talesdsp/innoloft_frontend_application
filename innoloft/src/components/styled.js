import {Form} from "@unform/web";
import styled, {css} from "styled-components";

export const Button = styled.button`
  font-size: 1.6rem;
  background-color: #fff;
  border-radius: 0.3rem;
  width: 14.4rem;
  height: 3.4rem;
  margin-top: 2rem;
  text-transform: uppercase;
  cursor: pointer;
  border: transparent;
  color: #fff;
  background-color: var(--main);

  ${(props) =>
    props.cancel &&
    css`
      background-color: #bbb;
    `}
`;

export const Buttons = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  opacity: 0;
  margin-top: 2rem;
  transition: 0.5s opacity ease;
  pointer-events: none;

  ${(props) =>
    props.show &&
    css`
      pointer-events: initial;
      opacity: 1;
    `}
`;

export const Unform = styled(Form)`
  display: none;
  position: relative;
  width: 80%;

  ${(props) =>
    props.active &&
    css`
      display: initial;
    `}
`;

export const FieldSet = styled.fieldset`
  display: flex;
  flex-direction: column;
  margin: auto;
  justify-content: center;
  align-content: center;
  position: relative;
  border: transparent;
  flex: 1;
  margin-bottom: 2rem;
  width: 100%;
  
  @media(min-width: 768px){
    flex-direction: row;
    ${(props) =>
      props.column &&
      css`
        flex-direction: column;
      `}
  }

  ${(props) =>
    props.column &&
    css`
      margin: 0;
      flex-direction: column;
    `}

  ${(props) =>
    props.country &&
    css`
      flex: 2;
    `}
    
  /* // ! VALIDATE PASSWORD */

  &::before {
    width: 8%;
    height: 0.3rem;
    left: 0;
    content: "";
    position: absolute;
    z-index: 10;
    transition: .5s background-color ease, .5s width ease;
    bottom: 5.8rem;

    @media(min-width: 768px){
    bottom: 0;
    }

  }

  ${(props) =>
    props.strength === "weak" &&
    css`
      &::before {
        background-color: red;
        width: 10%;
      }
    `}

  ${(props) =>
    props.strength === "medium" &&
    css`
      &::before {
        background-color: orange;
        width: 22%;
      }
    `}

  ${(props) =>
    props.strength === "solid" &&
    css`
      &::before {
        background-color: yellowgreen;
        width: 30%;
      }
    `}

  ${(props) =>
    props.strength === "strong" &&
    css`
      &::before {
        background-color: greenyellow;
        width: 40%;
      }
    `}

  ${(props) =>
    props.strength === "adamantium" &&
    css`
      &::before {
        background-color: springgreen;
        width: 99.5%;
        @media (min-width: 768px) {
          width: 49%;
        }
      }
    `}

/* // ! Confirm password */

  &::after{ 
    width: 0%;
    height: 0.3rem;
    left: 0%;
    content: "";
    position: absolute;
    bottom: 0;
    z-index:330;
    transition: .5s background-color ease, .5s width ease; 

    @media(min-width:768px){
      left: 50%;
    }

   }
    
  ${(props) =>
    props.equality === "not" &&
    css`
      &::after {
        background-color: red;
        width: 10%;
      }
    `}
    
  ${(props) =>
    props.equality === "equal" &&
    css`
      &::after {
        background-color: springgreen;
        width: 99%;
        @media (min-width: 768px) {
          width: 49%;
        }
      }
    `} 

  ${(props) =>
    props.form &&
    css`
      &::before,
      &::after {
        display: none;
      }
    `} 
`;

export const Label = styled.label`
  font-size: 1.8rem;
  color: rgba(40, 119, 98, 1);

  ${(props) =>
    props.password &&
    css`
      opacity: 0;
      pointer-events: none;
    `}

  ${(props) =>
    props.update &&
    css`
      opacity: 1;
      pointer-events: initial;
    `}
`;

export const Input = styled.input`
  line-height: 2;
  position: relative;
  font-size: 1.6rem;
  padding: 0 1rem;
  width: 99%;
  flex: 1;
  border: 1px solid transparent;
  transition: 0.5s border ease;

  ::placeholder {
    font-size: 1.3rem;
  }

  ${(props) =>
    props.password &&
    css`
      opacity: 0;
      pointer-events: none;
    `}

  ${(props) =>
    props.update &&
    css`
      opacity: 1;
      pointer-events: initial;
      border: 1px solid #ccc;
    `}

    &::before {
    width: 8%;
    height: 0.3rem;
    left: 0;
    content: "";
    position: absolute;
    bottom: 0;
    z-index: 10;
    transition: .5s background-color ease, .5s width ease;
  }

  ${(props) =>
    props.strength === "weak" &&
    css`
      &::before {
        background-color: red;
        width: 10%;
      }
    `}
  ${(props) =>
    props.strength === "medium" &&
    css`
      &::before {
        background-color: orange;
        width: 22%;
      }
    `}
  ${(props) =>
    props.strength === "solid" &&
    css`
      &::before {
        background-color: yellowgreen;
        width: 30%;
      }
    `}
  ${(props) =>
    props.strength === "strong" &&
    css`
      &::before {
        background-color: greenyellow;
        width: 40%;
      }
    `}
  ${(props) =>
    props.strength === "adamantium" &&
    css`
      &::before {
        background-color: springgreen;
        width: 49%;
      }
    `}


    ${(props) =>
      props.watch &&
      css`
        &::after {
          position: absolute;
          z-index: 33;
          right: 0;
        }
      `}
`;

export const Select = styled.select`
  font-size: 1.6rem;
  display: block;
  width: 99%;
  padding: 0.5rem 1rem;
  border: transparent;

  ${(props) =>
    props.update &&
    css`
      border: 1px solid #ccc;
    `}

  option {
    font-size: 1.6rem;
  }
`;

export const Rules = styled.ul`
  color: #777;
  display: none;
  list-style: square;
  margin-top: 2rem;
  ${(props) =>
    props.update &&
    css`
      display: block;
    `}
`;

export const Rule = styled.li`
  font-size: 1.2rem;
  margin-left: 2rem;
  ${(props) =>
    props.head &&
    css`
      margin-left: 0;
      font-size: 1.3rem;
      list-style: none;
    `}
`;

export const Test = styled(Rule)`
  list-style: none;
  color: #000;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const TestPassword = styled.input`
  font-size: 1.2rem;
  padding: 0.6rem;
`;

export const TestButton = styled.button`
  color: #000;
  width: 3rem;
  height: 3rem;
  font-size: 1.2rem;
  font-weight: bold;
  padding: 0.5rem;
  img {
    width: 100%;
  }
`;

export const Error = styled.span`
  color: red;
  font-size: 1.4rem;
  position: absolute;
  bottom: -2rem;
`;
