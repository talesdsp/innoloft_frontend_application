import React from "react";
import {useSelector} from "react-redux";
import styled, {css} from "styled-components";
import Card from "./components/Card";
import Footer from "./components/Footer";
import Menu from "./components/Menu";
import Navbar from "./components/Nav";
import SideBar from "./components/SideBar";

function App() {
  const {form} = useSelector((state) => state.formReducer);
  return (
    <>
      <Navbar />
      <Layout form={form === "edit" ? 1 : 0}>
        <SideBar />
        <Card />
      </Layout>
      <Menu />
      <Footer />
    </>
  );
}

const Layout = styled.div`
  margin-top: 12rem;
  position: relative;
  height: 55rem;

  ${(props) =>
    props.form &&
    css`
      height: 63rem;
    `}
`;

export default App;
