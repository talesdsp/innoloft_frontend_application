import {combineReducers} from "redux";
import formReducer from "./form";
import menuReducer from "./menu";
import messageReducer from "./message";
import notificationReducer from "./notification";

const logger = (state = {}, action) => {
  console.log(`%cRedux:`, "font-size: 2rem; color: purple", action);
  return state;
};

const rootReducer = combineReducers({
  formReducer,
  notificationReducer,
  messageReducer,
  menuReducer,
  logger
});

export default rootReducer;
