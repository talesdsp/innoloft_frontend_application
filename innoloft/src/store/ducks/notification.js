const TYPES = {
  receive: "@@notification/RECEIVE",
  read: "@@notification/READ"
};

export const notificationCreators = {
  receiveNotification: (body) => ({type: TYPES.receive, payload: body}),
  readNotification: () => ({type: TYPES.read})
};

const INITIAL_STATE = {
  notifications: []
};

const notificationReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.receive:
      return {notifications: [...state.notifications, action.payload]};
    case TYPES.read:
      return {notifications: []};
    default:
      return state;
  }
};

export default notificationReducer;
