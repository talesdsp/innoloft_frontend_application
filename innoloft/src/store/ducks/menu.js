const TYPES = {
  open: "@@menu/OPEN",
  close: "@@menu/CLOSE"
};

export const menuCreators = {
  openMenu: () => ({type: TYPES.open}),
  closeMenu: () => ({type: TYPES.close})
};

const INITIAL_STATE = {
  menu: "close"
};

const menuReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.open:
      return {menu: "open"};
    case TYPES.close:
      return {menu: "close"};
    default:
      return state;
  }
};

export default menuReducer;
