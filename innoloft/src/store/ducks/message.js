const TYPES = {
  receive: "@@message/RECEIVE",
  read: "@@message/READ"
};

export const messageCreators = {
  receiveMessage: (body) => ({type: TYPES.receive, payload: body}),
  readMessage: () => ({type: TYPES.read})
};

const INITIAL_STATE = {
  messages: []
};

const messageReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.receive:
      return {messages: [...state.messages, action.payload]};
    case TYPES.read:
      return {messages: []};
    default:
      return state;
  }
};

export default messageReducer;
