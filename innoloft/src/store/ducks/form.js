const TYPES = {
  edit: "@@form/EDIT",
  done: "@@form/DONE"
};

export const formCreators = {
  openForm: () => ({type: TYPES.edit}),
  closeForm: () => ({type: TYPES.done})
};

const INITIAL_STATE = {
  form: "done"
};

const formReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.edit:
      return {form: "edit"};
    case TYPES.done:
      return {form: "done"};
    default:
      return state;
  }
};

export default formReducer;
